package ch.ethz.matsim.students_fs18.schmid;

import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.api.core.v01.population.Population;

public class ReplaceMode {
	public void run(Population population, double pavPercentage) {
		for (Person person : population.getPersons().values()) {
			double randomThreshold = Math.random();
			if (pavPercentage >= randomThreshold){
				for (Plan plan : person.getPlans()) {
					for (PlanElement element : plan.getPlanElements()) {
						if (element instanceof Leg) {
							Leg leg = (Leg) element;
							if (leg.getMode().equals("car")) {
							leg.setMode("pav");
							}
						}
					}
				}
			}
		}
	}
}
