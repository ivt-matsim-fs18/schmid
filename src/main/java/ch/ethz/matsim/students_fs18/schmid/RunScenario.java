package ch.ethz.matsim.students_fs18.schmid;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Link;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.config.groups.QSimConfigGroup.VehiclesSource;
import org.matsim.core.controler.Controler;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.vehicles.VehicleType;
import org.matsim.vehicles.VehicleTypeImpl;

import ch.ethz.matsim.baseline_scenario.BaselineModule;
import ch.ethz.matsim.baseline_scenario.transit.BaselineTransitModule;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRoute;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRouteFactory;
import ch.ethz.matsim.baseline_scenario.zurich.ZurichModule;

public class RunScenario {
	static public void main(String[] args) {
		Config config = ConfigUtils.loadConfig(args[0]);
		double flowEfficiencyFactor = Double.parseDouble(args[1]);
        double pavPercentage = Double.parseDouble(args[2]);

		Scenario scenario = ScenarioUtils.createScenario(config);
		scenario.getPopulation().getFactory().getRouteFactories().setRouteFactory(DefaultEnrichedTransitRoute.class,
				new DefaultEnrichedTransitRouteFactory());
		ScenarioUtils.loadScenario(scenario);

		new ReplaceMode().run(scenario.getPopulation(), pavPercentage);

		// Add vehicle types
		VehicleType carType = new VehicleTypeImpl(Id.create("car", VehicleType.class));
		scenario.getVehicles().addVehicleType(carType);

		VehicleType avType = new VehicleTypeImpl(Id.create("pav", VehicleType.class));
		avType.setFlowEfficiencyFactor(flowEfficiencyFactor);
		scenario.getVehicles().addVehicleType(avType);

		// Additional config for private AV mode
		config.qsim().setMainModes(Arrays.asList("car", "pav"));
		config.plansCalcRoute().setNetworkModes(Arrays.asList("car", "pav"));
		config.qsim().setVehiclesSource(VehiclesSource.modeVehicleTypesFromVehiclesData);
		config.planCalcScore().getOrCreateModeParams("pav");

		config.travelTimeCalculator().setAnalyzedModes("car,pav");
		config.travelTimeCalculator().setSeparateModes(true);

		for (Link link : scenario.getNetwork().getLinks().values()) {
			Set<String> modes = new HashSet<>(link.getAllowedModes());

			if (modes.contains("car")) {
				modes.add("pav");
				link.setAllowedModes(modes);
			}
		}

		Controler controler = new Controler(scenario);

		controler.addOverridingModule(new BaselineModule());
		controler.addOverridingModule(new BaselineTransitModule());
		controler.addOverridingModule(new ZurichModule());

		controler.run();
	}
}
